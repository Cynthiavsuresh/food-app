package com.hcl.matrimony.service;

import java.util.List;

import com.hcl.matrimony.dto.ApiResponse;
import com.hcl.matrimony.dto.InterstedProfileDto;
import com.hcl.matrimony.dto.ProfileDto;

public interface ProfileService {
	ApiResponse updateProfile(String email, ProfileDto profileDto);

	List<ProfileDto> getMatches(String email, String gender, int ageFrom, int ageTo, String maritalStatus,
			String religion, String caste);

	ApiResponse showInterest(String email, InterstedProfileDto interestedProfile);
}
