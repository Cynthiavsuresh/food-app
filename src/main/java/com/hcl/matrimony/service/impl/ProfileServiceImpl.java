package com.hcl.matrimony.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.matrimony.dto.AddressDto;
import com.hcl.matrimony.dto.ApiResponse;
import com.hcl.matrimony.dto.InterstedProfileDto;
import com.hcl.matrimony.dto.ProfileDto;
import com.hcl.matrimony.entity.Address;
import com.hcl.matrimony.entity.Gender;
import com.hcl.matrimony.entity.InterestedProfile;
import com.hcl.matrimony.entity.MaritalStatus;
import com.hcl.matrimony.entity.Profile;
import com.hcl.matrimony.entity.Religion;
import com.hcl.matrimony.entity.Status;
import com.hcl.matrimony.entity.User;
import com.hcl.matrimony.exception.ResourceConflictExists;
import com.hcl.matrimony.exception.ResourceNotFound;
import com.hcl.matrimony.exception.UnauthorizedUser;
import com.hcl.matrimony.repository.InterestedProfileRepository;
import com.hcl.matrimony.repository.ProfileRepository;
import com.hcl.matrimony.repository.UserRepository;
import com.hcl.matrimony.service.ProfileService;

@Service
public class ProfileServiceImpl implements ProfileService {
	private final UserRepository userRepository;
	private final ProfileRepository profileRepository;
	private final InterestedProfileRepository interestedProfileRepository;
	
	private static final String EXCEPTION_MSG="User Not Found";

	@Autowired
	public ProfileServiceImpl(UserRepository userRepository, ProfileRepository profileRepository,
			InterestedProfileRepository interestedProfileRepository) {
		this.userRepository = userRepository;
		this.profileRepository = profileRepository;
		this.interestedProfileRepository = interestedProfileRepository;
	}

	@Override
	public ApiResponse updateProfile(String email, ProfileDto profileDto) {
		User user = userRepository.findByEmail(email).orElseThrow(() -> new ResourceNotFound(EXCEPTION_MSG));
		if (user.isLoggedIn()) {
			if (user.getEmail().equals(profileDto.getEmail())) {
				Address address = Address.builder().lane(profileDto.getAddress().getLane())
						.city(profileDto.getAddress().getCity()).state(profileDto.getAddress().getState())
						.country(profileDto.getAddress().getCountry()).build();
				Profile profile = Profile.builder().firstName(profileDto.getFirstName())
						.lastName(profileDto.getLastName()).age(profileDto.getAge()).caste(profileDto.getCaste())
						.email(profileDto.getEmail()).gender(profileDto.getGender().toString())
						.religion(profileDto.getReligion().toString())
						.maritalStatus(profileDto.getMaritalStatus().toString()).income(profileDto.getIncome())
						.zodiacSign(profileDto.getZodiacSign()).profession(profileDto.getProfession()).address(address)
						.build();
				profileRepository.save(profile);
				return ApiResponse.builder().message("Profile updated sucessfully").httpStatus(200l).build();
			} else
				throw new UnauthorizedUser("Unauthorized user");
		} else
			throw new UnauthorizedUser("Login to update the profile");
	}

	@Override
	public List<ProfileDto> getMatches(String email, String gender, int ageFrom, int ageTo, String maritalStatus,
			String religion, String caste) {

		User user = userRepository.findByEmail(email).orElseThrow(() -> new ResourceNotFound(EXCEPTION_MSG));
		if (user.isLoggedIn()) {
			List<Profile> profiles = profileRepository
					.findByGenderIgnoreCaseAndAgeBetweenAndMaritalStatusIgnoreCaseAndReligionIgnoreCaseAndCasteIgnoreCase(
							gender, ageFrom, ageTo, maritalStatus, religion, caste);
			Profile profile1 = profileRepository.findByEmail(email);
			profiles.remove(profile1);
			return profiles.stream()
					.map(profile -> ProfileDto.builder().firstName(profile.getFirstName())
							.lastName(profile.getLastName()).gender(Gender.valueOf(profile.getGender()))
							.religion(Religion.valueOf(profile.getReligion()))
							.maritalStatus(MaritalStatus.valueOf(profile.getMaritalStatus())).caste(profile.getCaste())
							.email(profile.getEmail()).age(profile.getAge()).income(profile.getIncome())
							.zodiacSign(profile.getZodiacSign()).profession(profile.getProfession())
							.address(getAddressDto(profile.getAddress())).build())
					.toList();
		} else
			throw new UnauthorizedUser("Login to view the matches");

	}

	private AddressDto getAddressDto(Address address) {
		return AddressDto.builder().lane(address.getLane()).city(address.getCity()).state(address.getState())
				.country(address.getCountry()).build();
	}

	@Override
	@Transactional
	public ApiResponse showInterest(String email, InterstedProfileDto interestedProfileDto) {
		User user = userRepository.findByEmail(email).orElseThrow(() -> new ResourceNotFound(EXCEPTION_MSG));
		if (user.isLoggedIn()) {
			List<InterestedProfile> interestedProfiles = interestedProfileDto.getEmails().stream()
					.map(interestedProfile -> {
						if (user.getEmail().equals(interestedProfile)) {
							throw new ResourceConflictExists("add favours to self is not valid ");
						}
						return InterestedProfile.builder().userEmail(email).interestedEmail(interestedProfile)
								.status(Status.NORESPONSE).build();
					}).toList();
			interestedProfileRepository.saveAll(interestedProfiles);
			return ApiResponse.builder().message("Profiles Added to interested").httpStatus(201l).build();
		}

		else
			throw new UnauthorizedUser("Login to add Profile favours");
	}

}
